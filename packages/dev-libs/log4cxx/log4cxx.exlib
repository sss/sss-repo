# Distributed under the terms of the GNU General Public License v2
# Copyright 2018 Gluzskiy Alexandr <sss@sss.chaoslab.ru>


require github [ user=apache project=logging-log4cxx tag=v$(ever replace_all _) ]
#TODO: version 0.10.0 require earlier autotools
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="Library of C++ classes for logging to files, syslog and other destinations"
HOMEPAGE="https://logging.apache.org/log4cxx/latest_stable/index.html"
LICENCES="Apache-2.0"
SLOT="0"
if ! ever is_scm; then
    DOWNLOADS="https://archive.apache.org/dist/logging/${PN}/${PV}/apache-${PNV}.tar.gz"
    WORK="${WORKBASE}/apache-${PNV}"
fi

MYOPTIONS="
    smtp [[ description = [ Send an e-mail when a specific logging event occurs, typically on errors or fatal errors ] ]]
    ( providers: unixodbc iodbc ) [[ number-selected = at-most-one ]]
"

DEPENDENCIES="
    build+run:
        dev-libs/apr:1
        dev-libs/apr-util:1
        smtp? ( net-libs/libesmtp )
        providers:unixodbc? ( dev-db/unixODBC )
        providers:iodbc? ( dev-db/libiodbc )
"

if ! ever at_least 0.10.1; then
    DEFAULT_SRC_PREPARE_PATCHES=(
        "${FILES}/${PN}-0.10.0-missing_includes.patch"
        "${FILES}/${PN}-0.10.0-gcc44.patch"
        "${FILES}/${PN}-0.10.0-unixODBC.patch"
        "${FILES}/${PN}-0.10.0-fix-c++14.patch"

    )
fi

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-doxygen
    --disable-html-docs
    --with-charset=utf-8
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'smtp SMTP libesmtp'
    'providers:unixodbc ODBC unixODBC'
    'providers:iodbc ODBC iODBC'
)

